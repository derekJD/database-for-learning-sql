-- Wywolanie skryptu w SQLPlus: @'D:\Bitbucket\baza\jargus.sql'
-- show
CLEAR SCREEN;
PROMPT ;
PROMPT logowanie jako STUDENT;
PROMPT ;

alter session set nls_date_format = 'YYYY-MM-DD HH24:MI';
set linesize 94
--
-- connect STUDENT@db2/STUDENTPASS;
--
--
PROMPT ;
PROMPT Kasowanie tabel;
PROMPT ;
---------------------------------------------;
--DELETE FROM 
--drop table 
---------------------------------------------;
DELETE FROM NNN_NAUCZ_PRZEDMIOT;
drop table NNN_NAUCZ_PRZEDMIOT;
---------------------------------------------;
DELETE FROM OCENY;
drop table OCENY;
---------------------------------------------;
DELETE FROM KKK_KLASA_PRZEDMIOT;
drop table KKK_KLASA_PRZEDMIOT;
---------------------------------------------;
DELETE FROM PRZEDMIOT;
drop table PRZEDMIOT;
---------------------------------------------;
DELETE FROM UWAGI;
drop table UWAGI; 
---------------------------------------------;
DELETE FROM NEWSY;
drop table NEWSY;
---------------------------------------------;
DELETE FROM NNK_NAUCZ_KLASA;
drop table NNK_NAUCZ_KLASA;
---------------------------------------------;
DELETE FROM UUU_UCZEN_KLASA;
drop table UUU_UCZEN_KLASA;
---------------------------------------------;
DELETE FROM KLASY;
drop table KLASY;
---------------------------------------------;
DELETE FROM OOO_OPIEKUN_UCZEN;
drop table OOO_OPIEKUN_UCZEN;
---------------------------------------------;
DELETE FROM NAUCZYCIELE;
drop table NAUCZYCIELE;
---------------------------------------------;
DELETE FROM UCZNIOWIE;
drop table UCZNIOWIE;
---------------------------------------------;
DELETE FROM OPIEKUNOWIE;
drop table OPIEKUNOWIE;
---------------------------------------------;


---------------------------------------------;
PROMPT;
PROMPT ---------------------------------;
PROMPT TWORZENIE TABEL
PROMPT ---------------------------------;
PROMPT;
---------------------------------------------;


PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE OPIEKUNOWIE
PROMPT----------------------------------------;
PROMPT ;
--
create table
OPIEKUNOWIE
(
OPI_ID_OPIEKUN number(4) NOT NULL,
OPI_IMIE varchar2(20) NOT NULL,
OPI_NAZWISKO varchar2(20) NOT NULL,
OPI_TELEFON varchar2(9)
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OPIEKUNOWIE
add constraint CSR_PK_OPIEKUNOWIE
primary key (OPI_ID_OPIEKUN);
----------------------------------------------

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE UCZNIOWIE;
PROMPT----------------------------------------;
PROMPT ;
--
create table
UCZNIOWIE
(
UCZ_ID_UCZEN 			number(4) 	 NOT NULL,
UCZ_IMIE 				varchar2(20) NOT NULL,
UCZ_DRUGIE_IMIE 		varchar2(20),
UCZ_NAZWISKO 			varchar2(20) NOT NULL,
UCZ_TELEFON_KOMORKOWY 	varchar2(9),
UCZ_EMAIL 				varchar2(50) NOT NULL,
UCZ_PESEL 				varchar2(11) NOT NULL
);


PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UCZNIOWIE
add constraint CSR_PK_UCZNIOWIE
primary key (UCZ_ID_UCZEN);
----------------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NAUCZYCIELE;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NAUCZYCIELE
(
NAU_ID_NAUCZYCIEL       number(4) 	 NOT NULL,
NAU_IMIE 				varchar2(20) NOT NULL,
NAU_NAZWISKO 			varchar2(20) NOT NULL,
NAU_TELEFON_KOMORKOWY 	varchar2(9)	 NOT NULL,
NAU_EMAIL 				varchar2(50) NOT NULL
);
----------------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NAUCZYCIELE
add constraint CSR_PK_NAUCZYCIELE
primary key (NAU_ID_NAUCZYCIEL);
----------------------------------------------;


PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE OOO_OPIEKUN_UCZEN;
PROMPT----------------------------------------;
PROMPT ;
--
create table
OOO_OPIEKUN_UCZEN
(
OOO_ID_OPIEKUN_UCZEN 	number(4) NOT NULL,
UCZ_ID_UCZEN 			number(4) NOT NULL,
OPI_ID_OPIEKUN 			number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OOO_OPIEKUN_UCZEN
add constraint CSR_PK_OOO_OPIEKUN_UCZEN
primary key (OOO_ID_OPIEKUN_UCZEN);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OOO_OPIEKUN_UCZEN add constraint CSR_FK_1_OOO_OPIEKUN_UCZEN
foreign key (UCZ_ID_UCZEN)
references UCZNIOWIE (UCZ_ID_UCZEN);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OOO_OPIEKUN_UCZEN add constraint CSR_FK_2_OOO_OPIEKUN_UCZEN
foreign key (OPI_ID_OPIEKUN)
references OPIEKUNOWIE (OPI_ID_OPIEKUN);
---------------------------------------------;

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE KLASY
PROMPT----------------------------------------;
PROMPT ;
--
create table
KLASY
(
KLA_ID_KLASY 	number(4) NOT NULL,
KLA_NAZWA_KLASY varchar2(20) NOT NULL,
KLA_ROK 		varchar2(20) NOT NULL,
KLA_OPIS 		varchar2(60) NOT NULL
);
---------------------------------------------;

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KLASY
add constraint CSR_PK_KLASY
primary key (KLA_ID_KLASY);
PROMPT----------------------------------------;
---------------------------------------------;


PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE UUU_UCZEN_KLASA;
PROMPT----------------------------------------;
PROMPT ;
--
create table
UUU_UCZEN_KLASA
(
UUU_ID_UCZEN_KLASA 	number(4) NOT NULL,
UCZ_ID_UCZEN 		number(4) NOT NULL,
KLA_ID_KLASY 		number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UUU_UCZEN_KLASA
add constraint CSR_PK_UUU_UCZEN_KLASA
primary key (UUU_ID_UCZEN_KLASA);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UUU_UCZEN_KLASA add constraint CSR_FK_1_UUU_UCZEN_KLASA
foreign key (UCZ_ID_UCZEN)
references UCZNIOWIE (UCZ_ID_UCZEN);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UUU_UCZEN_KLASA add constraint CSR_FK_2_UUU_UCZEN_KLASA
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);
PROMPT----------------------------------------;
---------------------------------------------;


PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NNK_NAUCZ_KLASA;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NNK_NAUCZ_KLASA
(
NNK_ID_NAUCZ_KLASA 	number(4) NOT NULL,
NAU_ID_NAUCZYCIEL 	number(4) NOT NULL,
KLA_ID_KLASY 	number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNK_NAUCZ_KLASA
add constraint CSR_PK_NNK_NAUCZ_KLASA
primary key (NNK_ID_NAUCZ_KLASA);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNK_NAUCZ_KLASA add constraint CSR_FK_1_NNK_NAUCZ_KLASA
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNK_NAUCZ_KLASA add constraint CSR_FK_2_NNK_NAUCZ_KLASA
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);
PROMPT----------------------------------------;
---------------------------------------------;


PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NEWSY;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NEWSY
(
NEW_ID_NEWSA 	number(4) NOT NULL,
NEW_TRESC 	varchar2(64) NOT NULL,
NEW_DATA 	DATE NOT NULL,
NAU_ID_NAUCZYCIEL number(4) NOT NULL,
KLA_ID_KLASY number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NEWSY
add constraint CSR_PK_NEWSY
primary key (NEW_ID_NEWSA);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NEWSY add constraint CSR_FK_1_NEWSY
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NEWSY add constraint CSR_FK_2_NEWSY
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);
PROMPT----------------------------------------;
---------------------------------------------;

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE PRZEDMIOT
PROMPT----------------------------------------;
PROMPT ;
--
create table
PRZEDMIOT
(
PRZ_ID_PRZEDMIOT 	number(4) NOT NULL,
PRZE_NAZWA 			varchar2(20) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table PRZEDMIOT
add constraint CSR_PK_PRZEDMIOT
primary key (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
----------------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE KKK_KLASA_PRZEDMIOT;
PROMPT----------------------------------------;
PROMPT ;
--
create table
KKK_KLASA_PRZEDMIOT
(
KKK_ID_KLASA_PRZEDMIOT 	number(4) NOT NULL,
PRZ_ID_PRZEDMIOT 		number(4) NOT NULL,
KLA_ID_KLASY 			number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KKK_KLASA_PRZEDMIOT
add constraint CSR_PK_KKK_KLASA_PRZEDMIOT
primary key (KKK_ID_KLASA_PRZEDMIOT);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KKK_KLASA_PRZEDMIOT add constraint CSR_FK_1_KKK_KLASA_PRZEDMIOT
foreign key (PRZ_ID_PRZEDMIOT)
references PRZEDMIOT (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KKK_KLASA_PRZEDMIOT add constraint CSR_FK_2_KKK_KLASA_PRZEDMIOT
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);
PROMPT----------------------------------------;
----------------------------------------------;


PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE UWAGI;
PROMPT----------------------------------------;
PROMPT ;
--
create table
UWAGI
(
UWA_ID_UWAGI 	number(4) NOT NULL,
UWA_TRESC 	varchar2(64) NOT NULL,
UCZ_ID_UCZEN 	number(4) NOT NULL,
NAU_ID_NAUCZYCIEL number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI
add constraint CSR_PK_UWAGI
primary key (UWA_ID_UWAGI);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI add constraint CSR_FK_1_UWAGI
foreign key (UCZ_ID_UCZEN)
references UCZNIOWIE (UCZ_ID_UCZEN);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI add constraint CSR_FK_2_UWAGI
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);
PROMPT----------------------------------------;
----------------------------------------------;


PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE OCENY;
PROMPT----------------------------------------;
PROMPT ;
--
create table
OCENY
(
OCE_ID_OCENY 		number(4) NOT NULL,
OCE_OCENA 			varchar2(20) NOT NULL,
OCE_KOMENTARZ 		varchar2(60) NOT NULL,
OCE_DATA 			DATE NOT NULL,
PRZ_ID_PRZEDMIOT	number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OCENY
add constraint CSR_PK_OCENY
primary key (OCE_ID_OCENY);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OCENY add constraint CSR_FK_OCENY
foreign key (PRZ_ID_PRZEDMIOT)
references PRZEDMIOT (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
----------------------------------------;

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NNN_NAUCZ_PRZEDMIOT;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NNN_NAUCZ_PRZEDMIOT
(
NNN_ID_NAUCZYCIEL_PRZEDMIOT 	number(4) NOT NULL,
PRZ_ID_PRZEDMIOT 				number(4) NOT NULL,
NAU_ID_NAUCZYCIEL 					number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNN_NAUCZ_PRZEDMIOT
add constraint CSR_PK_NNN_NAUCZ_PRZEDMIOT
primary key (NNN_ID_NAUCZYCIEL_PRZEDMIOT );
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNN_NAUCZ_PRZEDMIOT add constraint CSR_FK_1_NNN_NAUCZ_PRZEDMIOT
foreign key (PRZ_ID_PRZEDMIOT)
references PRZEDMIOT (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNN_NAUCZ_PRZEDMIOT add constraint CSR_FK_2_NNN_NAUCZ_PRZEDMIOT
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);
PROMPT----------------------------------------;
----------------------------------------------;


PROMPT DESCRIBE;
PROMPT----------------------------------------;
--
PROMPT Sprawdzamy czy prawidlowo utworzono tabele;
PROMPT ;
--
describe OPIEKUNOWIE;
describe UCZNIOWIE;
describe NAUCZYCIELE;
describe OOO_OPIEKUN_UCZEN;
describe KLASY;
describe UUU_UCZEN_KLASA;
describe NNK_NAUCZ_KLASA;
describe NEWSY;
describe UWAGI;

describe PRZEDMIOT;
describe KKK_KLASA_PRZEDMIOT;
describe OCENY;
describe NNN_NAUCZ_PRZEDMIOT;

---------------------------------------------------;
drop sequence SEQ_OPIEKUNOWIE;
create sequence SEQ_OPIEKUNOWIE increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_UCZNIOWIE;
create sequence SEQ_UCZNIOWIE increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_NAUCZYCIELE;
create sequence SEQ_NAUCZYCIELE increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_OOO_OPIEKUN_UCZEN;
create sequence SEQ_OOO_OPIEKUN_UCZEN increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_KLASY;
create sequence SEQ_KLASY increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_UUU_UCZEN_KLASA;
create sequence SEQ_UUU_UCZEN_KLASA increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_NNK_NAUCZ_KLASA;
create sequence SEQ_NNK_NAUCZ_KLASA increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_NEWSY;
create sequence SEQ_NEWSY increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_UWAGI;
create sequence SEQ_UWAGI increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_PRZEDMIOT;
create sequence SEQ_PRZEDMIOT increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_KKK_KLASA_PRZEDMIOT;
create sequence SEQ_KKK_KLASA_PRZEDMIOT increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_OCENY;
create sequence SEQ_OCENY increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;
drop sequence SEQ_NNN_NAUCZ_PRZEDMIOT;
create sequence SEQ_NNN_NAUCZ_PRZEDMIOT increment 
	by 1 start with 1 maxvalue 999999999 minvalue 1;
---------------------------------------------------;


PROMPT---------------------------------------------;
PROMPT;
PROMPT TRIGGERY: ;
PROMPT;
PROMPT--------------------------------------------;
create or replace trigger T_BI_OPIEKUNOWIE
before insert on OPIEKUNOWIE
for each row
begin
	if :new.OPI_ID_OPIEKUN is NULL then
		SELECT SEQ_OPIEKUNOWIE.nextval INTO :new.OPI_ID_OPIEKUN FROM dual;
	end if;
end;
/
--------------------------------------------

create or replace trigger T_BI_UCZNIOWIE
before insert on UCZNIOWIE
for each row
begin
	if :new.UCZ_ID_UCZEN is NULL then
		SELECT SEQ_UCZNIOWIE.nextval INTO :new.UCZ_ID_UCZEN FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_NAUCZYCIELE
before insert on NAUCZYCIELE
for each row
begin
	if :new.NAU_ID_NAUCZYCIEL is NULL then
		SELECT SEQ_NAUCZYCIELE.nextval INTO :new.NAU_ID_NAUCZYCIEL FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_OOO_OPIEKUN_UCZEN
before insert on OOO_OPIEKUN_UCZEN
for each row
begin
	if :new.OOO_ID_OPIEKUN_UCZEN is NULL then
		SELECT SEQ_OOO_OPIEKUN_UCZEN.nextval INTO :new.OOO_ID_OPIEKUN_UCZEN FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_KLASY
before insert on KLASY
for each row
begin
	if :new.KLA_ID_KLASY is NULL then
		SELECT SEQ_KLASY.nextval INTO :new.KLA_ID_KLASY FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_UUU_UCZEN_KLASA
before insert on UUU_UCZEN_KLASA
for each row
begin
	if :new.UUU_ID_UCZEN_KLASA is NULL then
		SELECT SEQ_UUU_UCZEN_KLASA.nextval INTO :new.UUU_ID_UCZEN_KLASA FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_NNK_NAUCZ_KLASA
before insert on NNK_NAUCZ_KLASA
for each row
begin
	if :new.NNK_ID_NAUCZ_KLASA is NULL then
		SELECT SEQ_NNK_NAUCZ_KLASA.nextval INTO :new.NNK_ID_NAUCZ_KLASA FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_NEWSY
before insert on NEWSY
for each row
begin
	if :new.NEW_ID_NEWSA is NULL then
		SELECT SEQ_NEWSY.nextval INTO :new.NEW_ID_NEWSA FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_PRZEDMIOT
before insert on PRZEDMIOT
for each row
begin
	if :new.PRZ_ID_PRZEDMIOT is NULL then
		SELECT SEQ_PRZEDMIOT.nextval INTO :new.PRZ_ID_PRZEDMIOT FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_KKK_KLASA_PRZEDMIOT
before insert on KKK_KLASA_PRZEDMIOT
for each row
begin
	if :new.KKK_ID_KLASA_PRZEDMIOT is NULL then
		SELECT SEQ_KKK_KLASA_PRZEDMIOT.nextval INTO :new.KKK_ID_KLASA_PRZEDMIOT FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_UWAGI
before insert on UWAGI
for each row
begin
	if :new.UWA_ID_UWAGI is NULL then
		SELECT SEQ_UWAGI.nextval INTO :new.UWA_ID_UWAGI FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_OCENY
before insert on OCENY
for each row
begin
	if :new.OCE_ID_OCENY is NULL then
		SELECT SEQ_OCENY.nextval INTO :new.OCE_ID_OCENY FROM dual;
	end if;
end;
/

---------------------------------------------

create or replace trigger T_BI_NNN_NAUCZ_PRZEDMIOT
before insert on NNN_NAUCZ_PRZEDMIOT
for each row
begin
	if :new.NNN_ID_NAUCZYCIEL_PRZEDMIOT is NULL then
		SELECT SEQ_NNN_NAUCZ_PRZEDMIOT.nextval INTO :new.NNN_ID_NAUCZYCIEL_PRZEDMIOT FROM dual;
	end if;
end;
/

---------------------------------------------


insert into OPIEKUNOWIE (OPI_IMIE, OPI_NAZWISKO, OPI_TELEFON) VALUES ('Mariusz', 'Kamienski',542345123);
insert into OPIEKUNOWIE (OPI_IMIE, OPI_NAZWISKO, OPI_TELEFON) VALUES ('Damian', 'Wykopski',542343223);

insert into UCZNIOWIE (UCZ_IMIE, UCZ_DRUGIE_IMIE, UCZ_NAZWISKO, UCZ_TELEFON_KOMORKOWY, UCZ_EMAIL, UCZ_PESEL) VALUES ('Marek','Wojciech','Kamienski',573123123,'marekwojkam@gmail.com',99032301393);
insert into UCZNIOWIE (UCZ_IMIE, UCZ_DRUGIE_IMIE, UCZ_NAZWISKO, UCZ_TELEFON_KOMORKOWY, UCZ_EMAIL, UCZ_PESEL) VALUES ('Tomek','Darek','Wykopski',534111333,'tomekwykop@o2.pl',99011301393);

insert into NAUCZYCIELE(NAU_IMIE, NAU_NAZWISKO, NAU_TELEFON_KOMORKOWY, NAU_EMAIL) VALUES ('Barbara','Kwiatek',511123321,'barbkwiatk@o2.pl');
insert into NAUCZYCIELE(NAU_IMIE, NAU_NAZWISKO, NAU_TELEFON_KOMORKOWY, NAU_EMAIL) VALUES ('Malgorzata','Borowiec',533323321,'borowoiecka@o2.pl');


insert into OOO_OPIEKUN_UCZEN (UCZ_ID_UCZEN, OPI_ID_OPIEKUN) VALUES (1,1);

insert into KLASY (KLA_NAZWA_KLASY, KLA_ROK, KLA_OPIS) VALUES ('Mat-fiz', '2016', 'Klasa z rozserzona matematyka i fizyka.');



insert into UUU_UCZEN_KLASA (UCZ_ID_UCZEN, KLA_ID_KLASY) VALUES (1,1);
insert into UUU_UCZEN_KLASA (UCZ_ID_UCZEN, KLA_ID_KLASY) VALUES (2,1);


insert into NNK_NAUCZ_KLASA (NAU_ID_NAUCZYCIEL, KLA_ID_KLASY) VALUES (1,1);
insert into NNK_NAUCZ_KLASA (NAU_ID_NAUCZYCIEL, KLA_ID_KLASY) VALUES (2,1);

insert into NEWSY (NEW_TRESC, NEW_DATA, NAU_ID_NAUCZYCIEL, KLA_ID_KLASY) VALUES ('KLASOWKA 20.04.2018', '2018-04-20', 1, 1);


insert into PRZEDMIOT (PRZE_NAZWA) VALUES ('Matematyka');
insert into PRZEDMIOT (PRZE_NAZWA) VALUES ('Fizyka');
PROMPT -----------------------------------;
PROMPT twoRZE KLASA_przedmiot;
PROMPT -----------------------------------;
insert into KKK_KLASA_PRZEDMIOT (PRZ_ID_PRZEDMIOT, KLA_ID_KLASY) VALUES (1,1);
insert into KKK_KLASA_PRZEDMIOT (PRZ_ID_PRZEDMIOT, KLA_ID_KLASY) VALUES (2,1);

PROMPT -----------------------------------;

insert into UWAGI (UWA_TRESC, UCZ_ID_UCZEN, NAU_ID_NAUCZYCIEL) VALUES ('Przeszkadzanie na lekcji', 1, 1);



insert into OCENY (OCE_OCENA, OCE_KOMENTARZ, OCE_DATA, PRZ_ID_PRZEDMIOT) VALUES (5, 'sprawdzian z trygonometrii', '2018-03-20', 1);

insert into NNN_NAUCZ_PRZEDMIOT (PRZ_ID_PRZEDMIOT, NAU_ID_NAUCZYCIEL) VALUES (1,1);



----------------------------------------
PROMPT ;
PROMPT END of SCRIPT;
-----------------------------------------
commit;
