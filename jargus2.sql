-- Wywolanie skryptu w SQLPlus:
-- @"D:\Bitbucket\baza\jargus.sql"


CLEAR SCREEN;
PROMPT ;
PROMPT logowanie jako STUDENT;
PROMPT ;
--
-- connect STUDENT@db2/STUDENTPASS;
--
--
PROMPT ;
PROMPT Kasowanie tabel;
PROMPT ;
--
-- UWAGA! Jesli tabele istnieja to zostana usuniete:
-- DELETE FROM USR; <- kasuje dane z tabeli
-- drop table USR; <- kasuje tabele
-- przy pierwszym wywołaniu skryptu polecenia drop i delete
DELETE FROM OOO_OPIEKUN_UCZEN;
drop table OOO_OPIEKUN_UCZEN;

DELETE FROM UUU_UCZEN_KLASA;
drop table UUU_UCZEN_KLASA;

DELETE FROM KKK_KLASA_PRZEDMIOT;
drop table KKK_KLASA_PRZEDMIOT;

DELETE FROM NNN_NAUCZ_PRZEDMIOT;
drop table NNN_NAUCZ_PRZEDMIOT;

DELETE FROM NNK_NAUCZ_KLASA;
drop table NNK_NAUCZ_KLASA;

DELETE FROM NEWSY;
drop table NEWSY;


DELETE FROM UWAGI;
drop table UWAGI;

--


DELETE FROM OPIEKUNOWIE;
drop table OPIEKUNOWIE;

DELETE FROM OCENY;
drop table OCENY;

DELETE FROM PRZEDMIOT;
drop table PRZEDMIOT;

DELETE FROM KLASY;
drop table KLASY;

DELETE FROM UCZNIOWIE;
drop table UCZNIOWIE;

DELETE FROM NAUCZYCIELE;
drop table NAUCZYCIELE;
--


PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE OPIEKUNOWIE
PROMPT----------------------------------------;
PROMPT ;
--
create table
OPIEKUNOWIE
(
OPI_ID_OPIEKUN number(4) NOT NULL,
OPI_IMIE varchar2(20) NOT NULL,
OPI_NAZWISKO varchar2(20) NOT NULL,
OPI_TELEFON varchar2(20) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OPIEKUNOWIE
add constraint CSR_PK_OPIEKUNOWIE
primary key (OPI_ID_OPIEKUN);

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE UCZNIOWIE;
PROMPT----------------------------------------;
PROMPT ;
--
create table
UCZNIOWIE
(
UCZ_ID_UCZEN 			number(4) 	 NOT NULL,
UCZ_IMIE 				varchar2(10) NOT NULL,
UCZ_DRUGIE_IMIE 		varchar2(10) NOT NULL,
UCZ_NAZWISKO 			varchar2(20) NOT NULL,
UCZ_TELEFON_KOMORKOWY 	varchar2(9)	 NOT NULL,
UCZ_EMAIL 				varchar2(50) NOT NULL,
UCZ_PESEL 				varchar2(11)
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UCZNIOWIE
add constraint CSR_PK_UCZNIOWIE
primary key (UCZ_ID_UCZEN);

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NAUCZYCIELE;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NAUCZYCIELE
(
NAU_ID_NAUCZYCIEL			number(4) 	 NOT NULL,
NAU_IMIE 				varchar2(10) NOT NULL,
NAU_NAZWISKO 			varchar2(20) NOT NULL,
NAU_TELEFON_KOMORKOWY 	varchar2(9)	 NOT NULL,
NAU_EMAIL 				varchar2(50) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NAUCZYCIELE
add constraint CSR_PK_NAUCZYCIELE
primary key (NAU_ID_NAUCZYCIEL);


PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE OOO_OPIEKUN_UCZEN;
PROMPT----------------------------------------;
PROMPT ;
--
create table
OOO_OPIEKUN_UCZEN
(
OOO_ID_OPIEKUN_UCZEN 	number(4) NOT NULL,
UCZ_ID_UCZEN 			number(4) NOT NULL,
OPI_ID_OPIEKUN 			number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OOO_OPIEKUN_UCZEN
add constraint CSR_PK_OOO_OPIEKUN_UCZEN
primary key (OOO_ID_OPIEKUN_UCZEN);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OOO_OPIEKUN_UCZEN add constraint CSR_FK_1_OOO_OPIEKUN_UCZEN
foreign key (UCZ_ID_UCZEN)
references UCZNIOWIE (UCZ_ID_UCZEN);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OOO_OPIEKUN_UCZEN add constraint CSR_FK_2_OOO_OPIEKUN_UCZEN
foreign key (OPI_ID_OPIEKUN)
references OPIEKUNOWIE (OPI_ID_OPIEKUN);

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE KLASY
PROMPT----------------------------------------;
PROMPT ;
--
create table
KLASY
(
KLA_ID_KLASY 	number(4) NOT NULL,
KLA_NAZWA_KLASY varchar2(20) NOT NULL,
KLA_ROK 		varchar2(20) NOT NULL,
KLA_OPIS 		varchar2(20) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KLASY
add constraint CSR_PK_KLASY
primary key (KLA_ID_KLASY);
PROMPT----------------------------------------;


PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE UUU_UCZEN_KLASA;
PROMPT----------------------------------------;
PROMPT ;
--
create table
UUU_UCZEN_KLASA
(
UUU_ID_UCZEN_KLASA 	number(4) NOT NULL,
UCZ_ID_UCZEN 		number(4) NOT NULL,
KLA_ID_KLASY 		number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UUU_UCZEN_KLASA
add constraint CSR_PK_UUU_UCZEN_KLASA
primary key (UUU_ID_UCZEN_KLASA);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UUU_UCZEN_KLASA add constraint CSR_FK_1_UUU_UCZEN_KLASA
foreign key (UCZ_ID_UCZEN)
references UCZNIOWIE (UCZ_ID_UCZEN);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UUU_UCZEN_KLASA add constraint CSR_FK_2_UUU_UCZEN_KLASA
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE PRZEDMIOT
PROMPT----------------------------------------;
PROMPT ;
--
create table
PRZEDMIOT
(
PRZ_ID_PRZEDMIOT 	number(4) NOT NULL,
PRZE_NAZWA 			varchar2(20) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table PRZEDMIOT
add constraint CSR_PK_PRZEDMIOT
primary key (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;


PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE KKK_KLASA_PRZEDMIOT;
PROMPT----------------------------------------;
PROMPT ;
--
create table
KKK_KLASA_PRZEDMIOT
(
KKK_ID_KLASA_PRZEDMIOT 	number(4) NOT NULL,
PRZ_ID_PRZEDMIOT 		number(4) NOT NULL,
KLA_ID_KLASY 			number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KKK_KLASA_PRZEDMIOT
add constraint CSR_PK_KKK_KLASA_PRZEDMIOT
primary key (KKK_ID_KLASA_PRZEDMIOT);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KKK_KLASA_PRZEDMIOT add constraint CSR_FK_1_KKK_KLASA_PRZEDMIOT
foreign key (PRZ_ID_PRZEDMIOT)
references PRZEDMIOT (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table KKK_KLASA_PRZEDMIOT add constraint CSR_FK_2_KKK_KLASA_PRZEDMIOT
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NNN_NAUCZ_PRZEDMIOT;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NNN_NAUCZ_PRZEDMIOT
(
NNN_ID_NAUCZYCIEL_PRZEDMIOT 	number(4) NOT NULL,
PRZ_ID_PRZEDMIOT 				number(4) NOT NULL,
NAU_ID_NAUCZYCIEL 					number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNN_NAUCZ_PRZEDMIOT
add constraint CSR_PK_NNN_NAUCZ_PRZEDMIOT
primary key (NNN_ID_NAUCZYCIEL_PRZEDMIOT );
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNN_NAUCZ_PRZEDMIOT add constraint CSR_FK_1_NNN_NAUCZ_PRZEDMIOT
foreign key (PRZ_ID_PRZEDMIOT)
references PRZEDMIOT (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNN_NAUCZ_PRZEDMIOT add constraint CSR_FK_2_NNN_NAUCZ_PRZEDMIOT
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);

PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NNK_NAUCZ_KLASA;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NNK_NAUCZ_KLASA
(
NNK_ID_NAUCZ_KLASA 	number(4) NOT NULL,
NAU_ID_NAUCZYCIEL 	number(4) NOT NULL,
KLA_ID_KLASY 	number(4) NOT NULL
);
--TABLESPACE STUDENT_DATA; <- standardowo uzytkownik student tworzy obiekty
-- w przestrzeni STUDENT_DATA
PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNK_NAUCZ_KLASA
add constraint CSR_PK_NNK_NAUCZ_KLASA
primary key (NNK_ID_NAUCZ_KLASA);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNK_NAUCZ_KLASA add constraint CSR_FK_1_NNK_NAUCZ_KLASA
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NNK_NAUCZ_KLASA add constraint CSR_FK_2_NNK_NAUCZ_KLASA
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);

PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE OCENY;
PROMPT----------------------------------------;
PROMPT ;
--
create table
OCENY
(
OCE_ID_OCENY 		number(4) NOT NULL,
OCE_OCENA 			varchar2(20) NOT NULL,
OCE_KOMENTARZ 		varchar2(20) NOT NULL,
OCE_DATA 			DATE NOT NULL,
PRZ_ID_PRZEDMIOT	number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OCENY
add constraint CSR_PK_OCENY
primary key (OCE_ID_OCENY);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table OCENY add constraint CSR_FK_OCENY
foreign key (PRZ_ID_PRZEDMIOT)
references PRZEDMIOT (PRZ_ID_PRZEDMIOT);
PROMPT----------------------------------------;
PROMPT ;

PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE UWAGI;
PROMPT----------------------------------------;
PROMPT ;
--
create table
UWAGI
(
UWA_ID_UWAGI 	number(4) NOT NULL,
UWA_TRESC 	varchar2(4) NOT NULL,
UCZ_ID_UCZEN 	number(4) NOT NULL,
NAU_ID_NAUCZYCIEL number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI
add constraint CSR_PK_UWAGI
primary key (UWA_ID_UWAGI);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI add constraint CSR_FK_1_UWAGI
foreign key (UCZ_ID_UCZEN)
references UCZNIOWIE (UCZ_ID_UCZEN);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI add constraint CSR_FK_2_UWAGI
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);

PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT CREATE TABLE NEWSY;
PROMPT----------------------------------------;
PROMPT ;
--
create table
NEWSY
(
NEW_ID_NEWSA 	number(4) NOT NULL,
NEW_TRESC 	varchar2(4) NOT NULL,
NEW_DATA 	DATE NOT NULL,
NAU_ID_NAUCZYCIEL number(4) NOT NULL,
KLA_ID_KLASY number(4) NOT NULL
);

PROMPT ;
PROMPT----------------------------------------;
PROMPT Primary Key;
PROMPT----------------------------------------;
PROMPT ;
alter table NEWSY
add constraint CSR_PK_NEWSY
primary key (UWA_ID_UWAGI);
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI add constraint CSR_FK_1_UWAGI
foreign key (NAU_ID_NAUCZYCIEL)
references NAUCZYCIELE (NAU_ID_NAUCZYCIEL);
PROMPT----------------------------------------;
PROMPT ;
PROMPT----------------------------------------;
PROMPT Foreign Key;
PROMPT----------------------------------------;
PROMPT ;
alter table UWAGI add constraint CSR_FK_2_UWAGI
foreign key (KLA_ID_KLASY)
references KLASY (KLA_ID_KLASY);

PROMPT DESCRIBE;
PROMPT----------------------------------------;
--
PROMPT Sprawdzamy czy prawidlowo utworzono tabele;
PROMPT ;
--
describe UCZNIOWIE;
describe OOO_OPIEKUN_UCZEN;
describe OPIEKUNOWIE;
describe UUU_UCZEN_KLASA;
describe KLASY;
describe PRZEDMIOT;
describe KKK_KLASA_PRZEDMIOT;
describe OCENY;
describe NAUCZYCIELE;
describe NNN_NAUCZ_PRZEDMIOT;
describe NNK_NAUCZ_KLASA;
describe UWAGI;
describe NEWSY;
--
--
PROMPT ;
PROMPT END of SCRIPT;
--
commit;